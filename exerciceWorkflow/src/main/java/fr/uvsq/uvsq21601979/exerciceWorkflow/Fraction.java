package fr.uvsq.uvsq21601979.exerciceWorkflow;

public final class Fraction implements Comparable<Fraction>{


	int num;
	int den;
	public Fraction()
	{
		this.num = 0;
		this.den = 1;
	}
	
	public Fraction(int num)
	{
		this.num = num;
		this.den = 1;
	}
	
	public Fraction(int num, int den )
	{
		this.num = num;
		this.den = den;
	}
	
	public static final Fraction ZERO = new Fraction(0,1);
	public static final Fraction UN = new Fraction(1,1);
	
	public int getNumerateur() {return num;}
    public int getDenominateur() {return den;}
    
    public double valeur() {return ((double)(num)/den);}
    
    public int compareTo(Fraction f)
    {
    	System.out.println("Si la méthode renvoie 0, les fractions sont égales. Si elle retroune 1, ma valeur est inférieur à l'autre fraction. Si elle retourne -1, ma valeur est supérieur à l'autre fraction.");
    	if (valeur() == f.valeur())
    	{ return 0;}
    	return valeur() < f.valeur() ? 1 : -1;
    }

}



